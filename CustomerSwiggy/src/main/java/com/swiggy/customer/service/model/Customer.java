package com.swiggy.customer.service.model;

import javax.persistence.Id;

public class Customer {
	
		private String name;
		@Id
		private String emailid;
		private String password;
		private Long phone;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getEmailid() {
			return emailid;
		}
		public void setEmailid(String emailid) {
			this.emailid = emailid;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public Long getPhone() {
			return phone;
		}
		public void setPhone(Long phone) {
			this.phone = phone;
		}
		@Override
		public String toString() {
			return "Customer [name=" + name + ", password=" + password + ", phone=" + phone + "]";
		}
		
		
}
