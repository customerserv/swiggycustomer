package com.swiggy.customer.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerSwiggyApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerSwiggyApplication.class, args);
	}
}
